<?php

namespace ADW\NiceSurveyBundle\Form\Type;

use ADW\NiceSurveyBundle\Entity\Answer;
use ADW\NiceSurveyBundle\Entity\Question;
use ADW\SonataMediaExtraBundle\Form\Type\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnswerType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('title', null, [
                'label' => false,
                'attr' => ['placeholder' => 'Ответ',],
            ])
            ->add('nextQuestion', new EnumeratedUnsavedEntityType(), [
                'label' => false,
                'placeholder' => '-- Следующий вопрос --',
                'data_provider' => [
                    'item_path' => 'form.parent.parent.parent.parent',
                    'label_path' => 'title',
                ],
            ])
            ->add('finishSurvey', null, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Завершить опрос',
                ],
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', Answer::class);
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix()
    {
        return 'adw_nice_survey__answer';
    }
}