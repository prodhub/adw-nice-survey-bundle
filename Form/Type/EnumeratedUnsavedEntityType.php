<?php

namespace ADW\NiceSurveyBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Данный FormType позволяет выводить в выпадающий список записи,
 * которые добавляются в текущей форме
 * и еще не сохранены в БД.
 */
class EnumeratedUnsavedEntityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        /*$builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) use ($options) {
            $form = $event->getForm();
            $path = $options['data_provider']['item_path'];
            $sourceForm = $this->getFormFromPath($form, $path);
            $dataArr = $sourceForm->getData();
            $index = $event->getData();

            if (!empty($index)) {
                $index = intval($index);

                $event->setData($dataArr[$index]);
                $event->getForm()->setData($dataArr[$index]);
            }
        });*/
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('data_provider', [
            'item_path' => 'form',
            'label_path' => 'title',
        ]);

        $resolver->setDefaults([
            'label' => false,
            'required' => false,
            'constraints' => [],
            'placeholder' => null,
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['placeholder'] = $options['placeholder'];

        list($choices, $value) = $this->transformData($view, $options);
        $view->vars['choices'] = $choices;
        $view->vars['value'] = $value;

        $view->vars['data_provider'] = $options['data_provider'];
    }

    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    public function getBlockPrefix()
    {
        return 'adw_nice_survey__enumerated_unsaved_entity';
    }

    /**
     * @param FormInterface $form
     * @param string $path
     * @return null|FormInterface
     */
    private function getFormFromPath($form, $path)
    {
        $pathArr = $this->parsePath($path);

        $formNode = $form;
        foreach ($pathArr as $item) {
            switch ($item) {
                case 'parent':
                    $formNode = $formNode->getParent();
                    break;
                default:
                    throw new \LogicException(
                        sprintf('Specified path part %s is not supported')
                    );
                    break;
            }
        }

        return $formNode;
    }

    /**
     * @param FormView $view
     * @param string $path
     * @return null|FormView
     */
    private function getViewFromPath(FormView $view, $path)
    {
        $pathArr = $this->parsePath($path);

        $viewNode = $view;
        foreach ($pathArr as $item) {
            switch ($item) {
                case 'parent':
                    $viewNode =$viewNode->parent;
                    break;
                default:
                    throw new \LogicException(
                        sprintf('Specified path part %s is not supported')
                    );
                    break;
            }
        }

        return $viewNode;
    }

    /**
     * @param string $path
     * @return array|null
     */
    private function parsePath($path)
    {
        $pathArr = explode('.', $path);
        $pathArr = array_filter($pathArr, [get_called_class(), "not_empty_validator"]);
        if (count($pathArr) == 0) {
            return null;
        }

        if ($pathArr[0] != 'form') {
            throw new \LogicException('First path item must be equals "form"');
        }

        array_shift($pathArr);
        return $pathArr;
    }

    function not_empty_validator($var)
    {
        return !empty($var);
    }

    /**
     * Так как данный FormType зависит от данных из другой формы,
     * для него не получится написать нормальный DataTransformer.
     * В трансформере никак не получить данные из родительской формы.
     * Поэтому мы пишем такие вот костыли, чтоб конвертировать объект в его порядковый индекс.
     *
     * @param FormView $view
     * @param array $options
     * @return array
     */
    private function transformData(FormView $view, array $options)
    {
        $path = $options['data_provider']['item_path'];
        $dataArr = $this->getViewFromPath($view, $path)->vars['data'];

        $choices = [];
        $obj = null;
        $index = 0;
        $value = null;
        foreach ($dataArr as $data) {
            $id = is_object($view->vars['value'])
                ? $view->vars['value']->getId()
                : $view->vars['value'];
            if (null != $view->vars['value'] && $data->getId() == $id) {
                $value = $index;
            }
            $choices[] = new ChoiceView($data, $index, strval($data));
            $index++;
        }

        return array($choices, $value);
    }
}