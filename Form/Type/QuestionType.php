<?php

namespace ADW\NiceSurveyBundle\Form\Type;

use ADW\NiceSurveyBundle\Entity\Question;
use ADW\SonataMediaExtraBundle\Form\Type\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('title', null, [
                'label' => 'Текст вопроса',
				'attr'=> [
					'class' => 'question_value'
				],
            ])
            ->add('images', 'collection', [
                'label' => 'Изображения',
                'type' => new QuestionImageType(),
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype_name' => '__Q_IMAGE__',
            ])
            ->add('answers', 'collection', [
                'label' => 'Ответы',
                'type' => new AnswerType(),
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype_name' => '__ANSWER__',
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', Question::class);
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix()
    {
        return 'adw_nice_survey__question';
    }
}