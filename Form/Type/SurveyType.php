<?php

namespace ADW\NiceSurveyBundle\Form\Type;

use ADW\NiceSurveyBundle\Entity\Question;
use ADW\NiceSurveyBundle\Entity\Survey;
use ADW\SonataMediaExtraBundle\Form\Type\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SurveyType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('title', null, [
                'label' => 'Название',
            ])
            ->add('status', null, [
                'label' => 'Статус',
            ])
            ->add('slug', null, [
                'label' => 'URL алиас',
            ])
            ->add('intro', null, [
                'label' => 'Вступительное слово',
            ])
            ->add('image', 'adw_sonata_media_extra__image_type', [
                'label' => 'Изображение',
                'context' => 'adw_nice_survey__survey',
                'provider' => 'sonata.media.provider.image',
                'new_on_update' => false,
                'resize_type' => ImageType::RESIZE_TYPE__OUTBOUND,
                'canvas' => ['width' => 200, 'height' => 200,],
            ])
            ->add('questions', 'collection', [
                'label' => 'Вопросы',
                'type' => new QuestionType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'prototype_name' => '__QUESTION__',
                'options' => array('data_class' => Question::class),
            ]);

        $builder
            ->add('btn_create_and_edit', 'submit', [
                'label' => '<i class="fa fa-save" aria-hidden="true"></i>
                Сохранить и редактировать',
                'attr' => ['class' => 'btn btn-success',],
            ])
            ->add('btn_create_and_list', 'submit', [
                'label' => '<i class="fa fa-save"></i>
                <i class="fa fa-list" aria-hidden="true"></i>
                Сохранить и вернуться к списку',
                'attr' => ['class' => 'btn btn-success',],
            ])
            ->add('btn_create_and_create', 'submit', [
                'label' => '<i class="fa fa-plus-circle" aria-hidden="true"></i>
                Сохранить и добавить новый',
                'attr' => ['class' => 'btn btn-success',],
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', Survey::class);
    }

	/**
	 * @inheritDoc
	 */
	public function getName()
	{
		return $this->getBlockPrefix();
	}

	/**
	 * @inheritDoc
	 */
	public function getBlockPrefix()
	{
		return 'adw_nice_survey__survey';
	}
}