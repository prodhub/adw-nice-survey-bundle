<?php

namespace ADW\NiceSurveyBundle\Form\Type;

use ADW\NiceSurveyBundle\Entity\QuestionImage;
use ADW\SonataMediaExtraBundle\Form\Type\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionImageType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('image', 'adw_sonata_media_extra__image_type', [
            'label' => false,
            'context' => 'adw_nice_survey__question',
            'provider' => 'sonata.media.provider.image',
            'new_on_update' => false,
            'resize_type' => ImageType::RESIZE_TYPE__OUTBOUND,
            'canvas' => ['width' => 200, 'height' => 200,],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', QuestionImage::class);
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix()
    {
        return 'adw_nice_survey__question_image';
    }
}