<?php

namespace ADW\NiceSurveyBundle\Model;

interface RespondentInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return string
     */
    function __toString();
}