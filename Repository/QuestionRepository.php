<?php

namespace ADW\NiceSurveyBundle\Repository;

use ADW\NiceSurveyBundle\Entity\Mark;
use ADW\NiceSurveyBundle\Entity\Question;
use Doctrine\ORM\EntityRepository;

class QuestionRepository extends EntityRepository
{
    /**
     * @param Mark $mark
     * @return Question|null
     */
    public function findOneNext(Mark $mark)
    {
        if ($mark->getAnswer()->isFinishSurvey()) {
            return null;
        }

        if (null !== ($nextQuestion = $mark->getAnswer()->getNextQuestion())) {
            return $nextQuestion;
        }

        $survey = $mark->getAnswer()->getQuestion()->getSurvey();
        $respondent = $mark->getRespondent();

        /** @var Question[] $answered */
        $answered = $this->createQueryBuilder('question')
            ->join('question.answers', 'answer')
            ->join('answer.marks', 'mark')
            ->where('mark.respondent = ' . $respondent->getId())
            ->getQuery()->getResult();
        $answeredIds = [];
        foreach ($answered as $item) {
            $answeredIds[] = $item->getId();
        }

        $qb = $this->createQueryBuilder('question');
        $qb->where($qb->expr()->eq('question.survey', $survey));
        $qb->andWhere(
            $qb->expr()->notIn('question.id', $answeredIds)
        );

        return $qb->setMaxResults(1)->getQuery()->getFirstResult();
    }
}