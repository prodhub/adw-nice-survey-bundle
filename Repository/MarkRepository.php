<?php

namespace ADW\NiceSurveyBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MarkRepository extends EntityRepository
{
    /**
     * @return integer
     */
    public function getCount()
    {
        return $this->createQueryBuilder('mark')
            ->select('COUNT(mark.id)')
            ->getQuery()->getSingleScalarResult();
    }
}