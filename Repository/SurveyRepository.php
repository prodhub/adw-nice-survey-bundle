<?php

namespace ADW\NiceSurveyBundle\Repository;

use ADW\NiceSurveyBundle\Entity\Survey;
use Doctrine\ORM\EntityRepository;

class SurveyRepository extends EntityRepository
{
    /**
     * @return null|Survey
     */
    public function findOneByActive()
    {
        $qb = $this->createQueryBuilder('survey');
        $qb
            ->where($qb->expr()->eq('survey.status', true));

        return $qb->getQuery()->getSingleResult();
    }
}