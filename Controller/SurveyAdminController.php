<?php

namespace ADW\NiceSurveyBundle\Controller;

use ADW\NiceSurveyBundle\Entity\Survey;
use ADW\NiceSurveyBundle\Form\Type\SurveyType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/adw/survey")
 */
class SurveyAdminController extends Controller
{
    /**
     * @Route("/list", name="adw_nice_survey__survey_admin__list")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function listAction()
    {
        /** @var Survey[] $surveys */
        $surveys = $this->getRepo()->findAll();

        return $this->render('ADWNiceSurveyBundle:SurveyAdmin:list.html.twig', [
            'template__layout' => $this->getAdminLayout(),
            'items' => $surveys,
        ]);
    }

    /**
     * @Route("/{id}/", name="adw_nice_survey__survey_admin__profile", defaults={"id": "create"})
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param Request $request
     * @param mixed $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction(Request $request, $id)
    {
        $object = null;

        if (null != $id && 'create' !== $id) {
            $object = $this->getRepo()->find($id);

            if (null == $object) {
                throw $this->createNotFoundException('Survey not found!');
            }
        } else {
            $object = new Survey();
        }

        $form = $this->createForm(new SurveyType(), $object, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($object->getQuestions() as $question) {
                foreach ($question->getAnswers() as $answer) {
                    $index = $answer->getNextQuestion();
                    if (!(empty($index) && $index !== '0') && is_numeric($index) && intval($index) >= 0) {
                        $index = intval($index);
                        $nq = $object->getQuestions()[$index];
                        $answer->setNextQuestion($nq);
                    }
                }
            }

            if ($object->getId()) {
                $em->merge($object);
            } else {
                $em->persist($object);
            }
            $em->flush();

            /** @var SubmitButton $submit */

            if (($submit = $form->get('btn_create_and_edit')) && $submit->isClicked()) {
                return $this->redirectToRoute(
                    'adw_nice_survey__survey_admin__profile',
                    ['id' => $object->getId(),]
                );
            }

            if (($submit = $form->get('btn_create_and_list')) && $submit->isClicked()) {
                return $this->redirectToRoute('adw_nice_survey__survey_admin__list');
            }

            if (($submit = $form->get('btn_create_and_create')) && $submit->isClicked()) {
                return $this->redirectToRoute(
                    'adw_nice_survey__survey_admin__profile'
                );
            }
        }

        return $this->render('ADWNiceSurveyBundle:SurveyAdmin:profile.html.twig', [
            'template__layout' => $this->getAdminLayout(),
            'form' => $form->createView(),
            'value' => $object,
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="adw_nice_survey__survey_admin__delete")
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param mixed $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id)
    {
        $survey = $this->getRepo()->find($id);

        if (null == $survey) {
            throw $this->createNotFoundException('Survey not found!');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($survey);
        $em->flush();

        $this->addFlash(
            'adw_nice_survey__survey_admin',
            'Survey # ' . $id . ' successfully removed!'
        );

        return $this->redirectToRoute('adw_nice_survey__survey_admin__list');
    }

    /**
     * @Route("/export/{id}/", name="adw_nice_survey__survey_admin__export_one")
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param Request $request
     * @param mixed $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function exportOneAction(Request $request, $id)
    {
        return null;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getRepo()
    {
        return $this->getDoctrine()->getRepository('ADWNiceSurveyBundle:Survey');
    }

    /**
     * @return string
     */
    private function getAdminLayout()
    {
        return trim(
            $this->getParameter('sonata.admin.configuration.templates')['layout'],
            '\'"'
        );
    }
}
