<?php

namespace ADW\NiceSurveyBundle\Controller;

use ADW\NiceSurveyBundle\Entity\Mark;
use ADW\NiceSurveyBundle\Repository\MarkRepository;
use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/adw/mark")
 */
class MarkAdminController extends Controller
{
    const ITEMS_LIMIT = 20;

    /**
     * @Route("/list", name="adw_nice_survey__mark_admin__list")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function listAction(Request $request)
    {
        $qb = $this->getRepo()->createQueryBuilder('mark');
        $qb
            ->select([
                'survey.id AS survey_id',
                'survey.title AS survey_title',
                'COUNT(DISTINCT mark.respondent) AS respondent_id_count',
                'COUNT(mark.id) AS mark_id_count',
            ])
            ->join('mark.question', 'question')
            ->join('question.survey', 'survey')
            ->groupBy('survey.id');

        $stat = $qb->getQuery()->getArrayResult();

        $totalMarksCount = $this->getRepo()->getCount();
        $totalPageCount = intval($totalMarksCount / self::ITEMS_LIMIT)
            + ($totalMarksCount % self::ITEMS_LIMIT > 0 ? 1 : 0);
        $pageNum = $request->query->get('items_page', 1);

        $offset = ($pageNum - 1) * self::ITEMS_LIMIT;
        $limit = self::ITEMS_LIMIT;

        /** @var Mark[] $marks */
        $marks = $this->getRepo()->findBy([], [
            'createdAt' => 'DESC',
            'id' => 'DESC',
        ], $limit, $offset);

        return $this->render('ADWNiceSurveyBundle:MarkAdmin:list.html.twig', [
            'template__layout' => $this->getAdminLayout(),
            'statictics' => $stat,
            'items' => $marks,
            'items_paging' => [
                'total' => $totalPageCount,
                'current' => $pageNum,
            ]
        ]);
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|EntityRepository|MarkRepository
     */
    private function getRepo()
    {
        return $this->getDoctrine()->getRepository('ADWNiceSurveyBundle:Mark');
    }

    /**
     * @return string
     */
    private function getAdminLayout()
    {
        return trim(
            $this->getParameter('sonata.admin.configuration.templates')['layout'],
            '\'"'
        );
    }
}