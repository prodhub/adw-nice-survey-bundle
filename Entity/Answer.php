<?php

namespace ADW\NiceSurveyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 * @ORM\Table(name="adw_nice_survey__answer")
 */
class Answer
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @var Question
     *
     * @ORM\ManyToOne(targetEntity="ADW\NiceSurveyBundle\Entity\Question")
     */
    protected $nextQuestion;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $finishSurvey = false;

    /**
     * @var Question
     *
     * @ORM\ManyToOne(
     *     targetEntity="ADW\NiceSurveyBundle\Entity\Question",
     *     inversedBy="answers"
     * )
     */
    protected $question;

    /**
     * @var Mark[]|ArrayCollection
     * @internal поле будет использовано исключительно в рамках QueryBuilder-а для построения запроса
     *
     * @ORM\OneToMany(
     *     targetEntity="ADW\NiceSurveyBundle\Entity\Mark",
     *     mappedBy="answer"
     * )
     */
    protected $marks;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Answer
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Question
     */
    public function getNextQuestion()
    {
        return $this->nextQuestion;
    }

    /**
     * @param Question $nextQuestion
     * @return Answer
     */
    public function setNextQuestion($nextQuestion)
    {
        $this->nextQuestion = $nextQuestion;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isFinishSurvey()
    {
        return $this->finishSurvey;
    }

    /**
     * @param boolean $finishSurvey
     * @return Answer
     */
    public function setFinishSurvey($finishSurvey)
    {
        $this->finishSurvey = $finishSurvey;

        return $this;
    }

    /**
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param Question $question
     * @return Answer
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @inheritDoc
     */
    function __toString()
    {
        return $this->getTitle();
    }
}