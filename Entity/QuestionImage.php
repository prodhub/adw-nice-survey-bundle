<?php

namespace ADW\NiceSurveyBundle\Entity;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 * @ORM\Table(name="adw_nice_survey__question_image")
 */
class QuestionImage
{
	use ORMBehaviors\Timestampable\Timestampable;

	/**
	 * @var int
	 *
	 * @ORM\Id()
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var Media
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="Sonata\MediaBundle\Model\MediaInterface",
	 *     cascade={"persist", "remove"}
	 * )
	 */
	protected $image;

	/**
	 * @var Question
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="ADW\NiceSurveyBundle\Entity\Question",
	 *     inversedBy="images"
	 * )
	 */
	protected $question;

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return Media
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * @param Media $image
	 * @return QuestionImage
	 */
	public function setImage($image)
	{
		$this->image = $image;

		return $this;
	}

	/**
	 * @return Question
	 */
	public function getQuestion()
	{
		return $this->question;
	}

	/**
	 * @param Question $question
	 * @return QuestionImage
	 */
	public function setQuestion($question)
	{
		$this->question = $question;

		return $this;
	}

	function __toString()
	{
		return $this->id . ' ' . $this->question->getId();
	}
}