<?php

namespace ADW\NiceSurveyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Sonata\MediaBundle\Model\Media;

/**
 * @ORM\Entity(repositoryClass="ADW\NiceSurveyBundle\Repository\SurveyRepository")
 * @ORM\Table(name="adw_nice_survey__survey")
 */
class Survey
{
    use ORMBehaviors\Timestampable\Timestampable;
    use ORMBehaviors\Sluggable\Sluggable;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $status = false;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $intro;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(
     *     targetEntity="Sonata\MediaBundle\Model\MediaInterface",
     *     cascade={"persist", "remove"}
     * )
     */
    protected $image;

    /**
     * @var Question[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="ADW\NiceSurveyBundle\Entity\Question",
     *     mappedBy="survey",
     *     cascade={"persist", "remove"},
     *     fetch="EXTRA_LAZY"
     * )
     */
    protected $questions;

    /**
     * Question constructor.
     */
    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return boolean
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param boolean $status
     * @return Survey
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Survey
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * @param string $intro
     * @return Survey
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;

        return $this;
    }

    /**
     * @return Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param Media $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return Question[]|ArrayCollection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param Question[]|ArrayCollection $questions
     * @return $this
     */
    public function setQuestions($questions)
    {
        $this->questions->clear();
        foreach ($questions as $question) {
            $this->addQuestion($question);
        }

        return $this;
    }

    /**
     * @param Question $question
     * @return $this
     */
    public function addQuestion($question)
    {
        if (!$this->questions->contains($question)) {
            $this->questions->add($question);
            $question->setSurvey($this);
        }

        return $this;
    }

    /**
     * @param Question $question
     * @return $this
     */
    public function removeQuestion($question)
    {
        $question->setSurvey(null);
        $this->questions->removeElement($question);

        return $this;
    }

    /**
     * @inheritDoc
     */
    function __toString()
    {
        return $this->getTitle();
    }

    /**
     * Returns an array of the fields used to generate the slug.
     *
     * @return array
     */
    public function getSluggableFields()
    {
        return ['title'];
    }
}