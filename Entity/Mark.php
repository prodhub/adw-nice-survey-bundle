<?php

namespace ADW\NiceSurveyBundle\Entity;

use ADW\NiceSurveyBundle\Model\RespondentInterface;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Ответ пользователя на вопрос
 *
 * @ORM\Entity(repositoryClass="ADW\NiceSurveyBundle\Repository\MarkRepository")
 * @ORM\Table(name="adw_nice_survey__mark")
 */
class Mark
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Question
     *
     * @ORM\ManyToOne(targetEntity="ADW\NiceSurveyBundle\Entity\Question")
     */
    protected $question;

    /**
     * @var Answer
     *
     * @ORM\ManyToOne(targetEntity="ADW\NiceSurveyBundle\Entity\Answer")
     */
    protected $answer;

    /**
     * @var RespondentInterface
     *
     * @ORM\ManyToOne(targetEntity="ADW\NiceSurveyBundle\Model\RespondentInterface")
     */
    protected $respondent;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param Question $question
     * @return Mark
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return Answer
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param Answer $answer
     * @return Mark
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * @return RespondentInterface
     */
    public function getRespondent()
    {
        return $this->respondent;
    }

    /**
     * @param RespondentInterface $respondent
     * @return Mark
     */
    public function setRespondent($respondent)
    {
        $this->respondent = $respondent;

        return $this;
    }

    /**
     * @inheritDoc
     */
    function __toString()
    {
        return sprintf('%s - %s - %s',
            $this->getRespondent(),
            $this->getQuestion(),
            $this->getAnswer()
        );
    }
}