<?php

namespace ADW\NiceSurveyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Sonata\MediaBundle\Model\Media;

/**
 * @ORM\Entity(repositoryClass="ADW\NiceSurveyBundle\Repository\QuestionRepository")
 * @ORM\Table(name="adw_nice_survey__question")
 */
class Question
{
	use ORMBehaviors\Timestampable\Timestampable;

	/**
	 * @var int
	 *
	 * @ORM\Id()
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string")
	 */
	protected $title;

	/**
	 * @var QuestionImage[]|ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *     targetEntity="ADW\NiceSurveyBundle\Entity\QuestionImage",
	 *     mappedBy="question",
	 *     cascade={"persist", "remove"}
	 * )
	 */
	protected $images;

	/**
	 * @var Survey
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="ADW\NiceSurveyBundle\Entity\Survey",
	 *     inversedBy="questions"
	 * )
	 */
	protected $survey;

	/**
	 * @var Answer[]|ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *     targetEntity="ADW\NiceSurveyBundle\Entity\Answer",
	 *     mappedBy="question",
	 *     cascade={"persist", "remove"}
	 * )
	 */
	protected $answers;

	/**
	 * Question constructor.
	 */
	public function __construct()
	{
	    $this->images = new ArrayCollection();
		$this->answers = new ArrayCollection();
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return Question
	 */
	public function setTitle($title)
	{
		$this->title = $title;

		return $this;
	}

	/**
	 * @return QuestionImage[]|ArrayCollection
	 */
	public function getImages()
	{
		return $this->images;
	}

	/**
	 * @param QuestionImage[]|ArrayCollection $images
	 * @return Question
	 */
	public function setImages($images)
	{
//		$this->images = $images;

		$this->images->clear();
		foreach ($images as $image) {
			$this->addImage($image);
		}

		return $this;
	}

	/**
	 * @param QuestionImage $image
	 * @return $this
	 */
	public function addImage($image)
	{
		if (!$this->images->contains($image)) {
			$image->setQuestion($this);
			$this->images->add($image);
		}

		return $this;
	}

	/**
	 * @param QuestionImage $image
	 * @return $this
	 */
	public function removeImage($image)
	{
		$image->setQuestion(null);
		$this->images->removeElement($image);

		return $this;
	}

	/**
	 * @return Survey
	 */
	public function getSurvey()
	{
		return $this->survey;
	}

	/**
	 * @param Survey $survey
	 * @return Question
	 */
	public function setSurvey($survey)
	{
		$this->survey = $survey;

		return $this;
	}

	/**
	 * @return Answer[]|ArrayCollection
	 */
	public function getAnswers()
	{
		return $this->answers;
	}

	/**
	 * @param Answer[]|ArrayCollection $answers
	 * @return Question
	 */
	public function setAnswers($answers)
	{
		$this->answers->clear();
		foreach ($answers as $answer) {
			$this->addAnswer($answer);
		}

		return $this;
	}

	/**
	 * @param Answer $answer
	 * @return $this
	 */
	public function addAnswer($answer)
	{
		if (!$this->answers->contains($answer)) {
			$this->answers->add($answer);
			$answer->setQuestion($this);
		}

		return $this;
	}

	/**
	 * @inheritDoc
	 */
	function __toString()
	{
		return $this->getTitle();
	}
}