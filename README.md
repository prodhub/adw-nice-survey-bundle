# ADW Nice Survey Bundle #


### Installation ###

Add repository

    "repositories": [
        {
          "type": "vcs",
          "url": "https://bitbucket.org/prodhub/adw-nice-survey-bundle.git"
        },
        {
          "type": "vcs",
          "url": "https://bitbucket.org/prodhub/adw-sonata-media-extra-bundle.git"
        }
    ]

Install vendor

    $ composer require adw/nice-survey-bundle

Setup target entities

    # app/config/config.yml
    doctrine:
        ...
        orm:
            ...
            resolve_target_entities:
                Sonata\MediaBundle\Model\MediaInterface: Application\Sonata\MediaBundle\Entity\Media
                ADW\NiceSurveyBundle\Model\RespondentInterface: AppBundle\Entity\Customer

Enable bundle for framework

###### app/AppKernel.php
    ...
    class AppKernel extends Kernel
    {
        public function registerBundles()
        {
            $bundles = array(
                ...
                new ADW\SonataMediaExtraBundle\ADWSonataMediaExtraBundle(),
                new ADW\NiceSurveyBundle\ADWNiceSurveyBundle(),
                ...
            );

Update DB schema

    $ php app/console doctrine:schema:update --force

Setup media contexts

    sonata_media:
        contexts:
            ...
            adw_nice_survey__survey:
                providers:
                    - sonata.media.provider.image
                formats: ~
            adw_nice_survey__question:
                providers:
                    - sonata.media.provider.image
                formats: ~
            adw_nice_survey__answer:
                providers:
                    - sonata.media.provider.image
                formats: ~

Fix if you use SonataClassification

    $ php app/console sonata:media:fix-media-context

Add routing

###### app/config/routing.yml

    adw_nice_survey_admin:
        resource: "@ADWNiceSurveyBundle/Controller/"
        type:     annotation
        prefix:   /admin


### Common information ###

Routes list

* **adw_nice_survey__survey_admin__list** - list of all surveys
* **adw_nice_survey__survey_admin__profile** - form for creation or editing existing survey
* **adw_nice_survey__survey_admin__delete** - remove existing survey
* **adw_nice_survey__survey_admin__export_one** - export data about survey
